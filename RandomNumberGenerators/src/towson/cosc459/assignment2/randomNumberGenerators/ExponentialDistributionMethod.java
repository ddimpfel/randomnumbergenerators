package towson.cosc459.assignment2.randomNumberGenerators;


/**
 * @author Dominick Dimpfel
 *
 */
public class ExponentialDistributionMethod {

	public static double[] expDistributedRand(double[] uniformRandomNumbers, double mean) {
		double[] randomNumbers = new double[RandomNumberGenerators.MAX];
		
		for(int i=1; i<RandomNumberGenerators.MAX; i++) 
			randomNumbers[i] = Math.log(uniformRandomNumbers[i])/(-mean);
		
		return randomNumbers;
	}
	
}
