package towson.cosc459.assignment2.randomNumberGenerators;

import java.util.Scanner;
import java.util.Timer;

public class RandomNumberGenerators {

	protected static final int MAX = 100;
	private static double[] randomNumbers;
	private static double xInitial = 38, a = 214013, c = 2531011, m = 2147483647; // MicroSoft's values for random numbers (with 38 as seed)
	private static double mean;
	private static int input;
	private static char again;
	private static boolean running = true;
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);

		while(running) {
			System.out.println("Which Random Number Generator would you like to use: ");
			System.out.println("1 - Multiplicative Congruential Method");
			System.out.println("2 - Exponential Distribution");
			System.out.println("3 - Exit");
			System.out.print("Please enter the integer of the generator: ");
			
			input = (int) scan.nextDouble();
			
			switch(input) {
				case 1:
					System.out.println("Enter initial x value: ");
					xInitial = scan.nextDouble();
					System.out.println("the a value: ");
					a = scan.nextDouble();
					System.out.println("the c value: ");
					c = scan.nextDouble();
					System.out.println("the m value: ");
					m = scan.nextDouble();
					randomNumbers = MultiplicativeCongruentialMethod.multiCongruentialRand(xInitial, a, c, m);
					uniform();
					break;
				case 2:
					randomNumbers = MultiplicativeCongruentialMethod.multiCongruentialRand(xInitial, a, c, m);
					uniform();
					
					System.out.print("Enter mean: ");
					mean = scan.nextDouble();
					randomNumbers = ExponentialDistributionMethod.expDistributedRand(randomNumbers, mean);
					
					break;
				default: 
					running = false;
			}
			if(running) {
				printArray();
				System.out.print("Return to menu? (Y/N) ");
				again = scan.next().charAt(0);
				if(again == 'N' || again == 'n')
					running = false;
				System.out.println();
			}
			
		}
				
	}
	
	private static void printArray() {
		for(int i=0; i<MAX; i++) {
			System.out.printf("%.3f ", randomNumbers[i]);
			if((i+1) % 20 == 0)
				System.out.println();
		}
		System.out.print("\n\n");
	}

	private static void uniform() {
		for(int i=0; i<MAX; i++)
			randomNumbers[i] /= m;
	}

}
