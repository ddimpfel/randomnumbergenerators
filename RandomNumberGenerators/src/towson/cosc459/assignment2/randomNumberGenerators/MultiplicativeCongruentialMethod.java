package towson.cosc459.assignment2.randomNumberGenerators;


/**
 * @author Dominick Dimpfel
 *
 */
public class MultiplicativeCongruentialMethod {
	
	public static double[] multiCongruentialRand(double xInitial, double a, double c, double m) {
		double[] randomNumbers = new double[RandomNumberGenerators.MAX];
		
		randomNumbers[0] = xInitial;
		for(int i=1; i<RandomNumberGenerators.MAX; i++) 
			randomNumbers[i] = ((a*randomNumbers[i-1]) + c) % m;
		
		return randomNumbers;
	}
	
}
